// let data = require("./data");
import data from "./data.js";

console.log(data);

let bodyele = document.querySelector(`body`);
let heading = document.createElement(`header`);
let h1 = document.createElement("h1");
h1.innerText = "Books";
document.body.appendChild(heading);
heading.appendChild(h1);
heading.style.textAlign = "center";
heading.style.background = "blue";
let mainDiv = document.createElement("div");
mainDiv.classList.add("main");
mainDiv.style.width = "100%";
mainDiv.style.height = "800px";
document.body.appendChild(mainDiv);
mainDiv.style.display = "flex";
mainDiv.style.flexGrow = "wrap";
for (let index = 0; index < 8; index++) {
  let div = document.createElement("div");
  mainDiv.append(div);
  div.classList.add("image-containt");
  div.style.width = "50%";
  div.style.height = "400px";
  div.style.margin = "10px";
  div.style.background = "gray";
  //   div.style.display = "inline-block";
  div.style.display = "flex";
  div.style.alignItems = "center";
  div.style.flexDirection = "column";
}

let allInnerDiv = document.querySelectorAll(".image-containt");
for (let i = 0; i < allInnerDiv.length; i++) {
  let image = document.createElement("img");
  image.src = data.books[i].image;
  allInnerDiv[i].append(image);
  image.style.width = "40%";
  image.style.height = "30%";
  let headingThree = document.createElement("h3");
  headingThree.innerText = data.books[i].title;
  allInnerDiv[i].append(headingThree);
  let writer = document.createElement("h4");
  writer.innerText = data.books[i].author;
  allInnerDiv[i].append(writer);
  let btn = document.createElement("button");
  btn.innerText = "Buy Now";
  btn.style.width = "80px";
  btn.style.height = "40px";
  btn.style.background = "blue";
  allInnerDiv[i].append(btn);
}
